var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var maxSize = 0.25
var resolution = 64
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < resolution; i++) {
    push()
    translate(windowWidth * 0.5 + boardSize * maxSize * sin(frame * 6 * i * 0.1), windowHeight * 0.5 + boardSize * maxSize * cos(frame * 6 * i * 0.1))
    fill(255 * (i % 2))
    noStroke()
    ellipse(0, 0, boardSize * maxSize * (1 - i / resolution))
    pop()

    push()
    translate(windowWidth * 0.5 + boardSize * maxSize * sin(frame * 6 * i * 0.1 + Math.PI), windowHeight * 0.5 + boardSize * maxSize * cos(frame * 6 * i * 0.1 + Math.PI))
    fill(255 * (i % 2))
    noStroke()
    ellipse(0, 0, boardSize * maxSize * (1 - i / resolution))
    pop()
  }

  frame += deltaTime * 0.0001
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
